// This is the demonstration to show how to import clients dll and use all the functions present in the clients dll along with this program.
//Client : The person who is using this program.
//dllname : The clients will provide this dll.


#include<stdio.h>
#include<conio.h>
#include<windows.h>
#include<String.h>
#include"headerfile.h"

//defining function pointer.
typedef void (*myfunc)(void);

//calling_function is use for calling functions from DLL
void calling_function(char **function_name, int ch, HINSTANCE hinst)
{
    myfunc func1; 
    func1 = (myfunc)GetProcAddress(hinst,function_name[ch]);  //function_name[] is an string array in which we store name of functions
    func1();

}
//getIndex() is use to getIndex index value of char which stored in given array
int getIndex(char* ary, char c, int bound)
{
    for(int i=0;i<bound;i++)
    {
        if(ary[i]==c)
        {
            return i;
            break;
        }
    }
    return -1;
}
int main()
{
    HINSTANCE hinst;                      // hinstance is declared
    myfunc func1;     
    char dllname[100];                   // Array for storing clients dll name.
    char ch;                             
    int size;                           // variable for storing number of functions 
    int count=0;                                                    
    int var=1;
    printf("Enter dll name :  ");
    scanf("%s",&dllname);

    printf("\nEnter number of function's in your dll : ");    // Number of functions present in clients dll.
    scanf("%d",&size);
    char key[size];                                                   
    char *function_name[size];         // Pointer Array is to store the function names present in clients dll.


    hinst = LoadLibrary(dllname);      //  LoadLibrary is used to  load dll.
    if(hinst)                          //  if dll is present 
    {
        for(int i=0;i<size;i++) 
        {
            char temp[50];
            function_name[i] = (char*)malloc(sizeof(char) * 20);   
            printf("\nEnter %d function name :  ",i+1);            
            scanf("%s",&temp);                               // Function names are taken from client and stored in function_name array
            strcpy(function_name[i],temp);                       

            func1 = (myfunc)GetProcAddress(hinst,function_name[i]);  // Retrieves the address of an exported function  from the specified DLL
            if(func1)                                                // if the address is valid the given function is valid
            {
                printf("Valid Function");
            }
            else                 // if the address is not valid the counter will increment
            {
                count++;
            }
        }           
    }
    else                                                          
    {
        printf("\n Dll not present");    // if the loaded dll is not present else is executed
    }

    if(count > 0)                        // if the count variable is greater than 0 then all functions present in the dll doesn't match with the function name given by client.
    {
        printf("\n\n Your dll is not compatible with our application\n");
    }
    else                                // if the count variable is equal to 0 then all functions present in the dll matches with the function name given by client.
    {
        printf("\n \n Your dll is compatible with our application\n");


    // Till now we have imported the dll  and seen what all  functions are specified are present in the dll or not.
    // Now we will use this dll function using below lines of codes.    



    printf("\n");
    printf("\n --- Setup your controls ---");
    for(int i=0;i<size;i++)               // This is the loop for setting the keys for various functions.
    {
    printf("\n %d) Key for %s : ",i+1,function_name[i]);
    scanf(" %c",&key[i]);
    }


    printf("\n --- Perform Action --- \n press TAB for terminate !!!\n"); 



    while(var)
    {
        printf("\n Enter your key : ");                             
        ch = getch();
        printf("%c\n",ch);
        int case_val = getIndex(key,ch,size);               // getIndex function will be called and an index value will be returned 
        if(case_val!=-1)                                    
        {calling_function(function_name,case_val,hinst);}   // This calling_function will call  function from the specified dll and output will be printed from that function.
        else{
            if(ch=='\t'){                                   // If tab key is pressed the program will terminate.
                var=0;
                break;
            }
            printf("please enter valid key !!!\n");}        //if key entered doesn't match with any of the set keys this will be printed.
    }
    }

    return 0;
}
‌